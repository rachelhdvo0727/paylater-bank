#!/bin/sh

echo "Running entrypoint..."

echo "Environment: Development"
echo "Make migrations & migrate"
python manage.py makemigrations --merge
python manage.py migrate --noinput
python manage.py demodata

echo "Run server"
python manage.py runserver 127.0.0.1:8000


