Installation
============

This page contains step-by-step on how to install and set up the Taco Bank application.

Prerequisites
_____________

You must have the following installed before proceeding to the next step:

* Docker
* Docker compose

Visit `Docker Documentations <https://docs.docker.com/>`_ for guide on how to install Docker


Installation
____________

#. Tunnel into your virtual private server::

    ssh -L 8000:127.0.0.1:8000 [servername]

#. Create an empty directory
#. Clone the following repository with SSH in the newly created directory: git@gitlab.com:rachelhdvo0727/banking-system.git
#. Build Docker image with all the logs::
    
    docker image build -t banking-system:latest . 
    
    // OR using docker-compose command
    // --build: to ensure images are build before starting containers
    docker-compose up -d --build 

#. Run the built image. Use ``-d`` flag if you want to run containers in the background, without real-time logs::

    docker-compose up

#. When done, remember to bring down the container::

    docker-compose down


Next time, when you work on the project again. You can use the command from step 5


If you run into trouble and you want to build the image from scratch, you can delete all volumes and their data using this command below::

    docker volume rm $(docker volume ls -q)

