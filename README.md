### Taco Bank
From KEA Bank

Mandatory Assignment 1 project for Back-end with Python & Django elective course at KEA (Copenhagen School of Design and Technology).

After database migration you will need to run the provisioning script:

$ python manage.py provision

To add demo data (the bank has some money):

$ python manage.py demodata


The project is now packaged for development environment, using Docker. Click [here](https://rachelhdvo0727.gitlab.io/banking-system/) for full documentation
