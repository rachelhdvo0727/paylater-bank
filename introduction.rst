Introduction
================

Welcome to Taco Banking System. Our project is a Python & Django application with the purpose of recreating a real life banking system. It includes a double-entry bookkeeping and two types of user within this system: bank employees and customers.

Basic features
______________

Taco Bank has the following basic features:

* Customers can:

  * Have any number of accounts

  * Have loans

  * Pay out loans

  * Have rankings

  * Make bank transfers if the fund is sufficient

  * View their accounts, account balances and movements


* Bank employees can:

  * View all customers and customer accounts

  * Create new customers and accounts

  * Edit customer rankings

Additional features
___________________

Additionally, Taco Bank includes these features to make it more secured and intuitive:

* Customers can:

  * Enable Two-Factor Authentication

  * Make external bank transfers to other banks

  * Download bank statement as PDF

* Bank can:

  * Charge interest for loans
