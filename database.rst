Database System
===============

Taco Bank uses a relational database, because it has a clear structure in the form of tables. Data can be easily organised into categories, which are more comprehensive for humans, and relationships between tables are clear. It simulates the real banking system nowadays.

A relational database has constraints and the source of truth is always the primary key, which creates relationships between tables. There are not a lot of extra details to be given in order to do something in a bank. If one needs to make a bank transfer to another person, a bank account number is the crucial thing that is needed. When a bank clerk would like to find details of a customer, they would only need the customer’s personal ID. 

A relational database utilises Set Theory in mathematics. JOIN operations are built upon this theory. Data tables can be partly or fully merged with one another to show a full overview, as long as they share the same key.

Developers may create a new table in models.py
