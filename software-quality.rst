Software Quality Assurance
==========================

Software Quality Assurance is required for the project to enforce the highest quality to the code. It ensures this project is free of errors and mistakes while following the common coding standards in Python and the standard that is set by the team. By having this quality assurance, it allows developers to identify flaws in the code and design throughout the development process in order to increase time management, efficiency and most importantly, to make sure the code works on all machines. 

Coding standards
________________

Since the project involves more than one developer, there is a coding standard that all developers must abide by and it is defined by a set of rules. These rules are created to always keep the code uniform and follow the same structure. It can be achieved by: 

* Basics:

  * The naming convention must be ``lower_case_with_underscore``

  * All names must be descriptive and clear

  * Comments must be added to explain that part of the code

  * Comments must be relevant and up to the date with the code described

  * There must a space after the *#* for the comment

  * A folder must only contains relevant files to the folder’s subject

* Indentation:

  * Use 4 tabs per indentation level

  * Avoid mixing tabs and spaces for indentation

  * Line break after a binary operator

* Line length:

  * Try to limit all lines to 75 characters

  * Increases comprehension and easier to understand

  * Limiting characters to 75 allows several files to be opened side by side

* Import:

  * All imports must be done on top of the file

  * Imports must be in separate lines

  * Imports should be grouped into an order:

    * standard library import

    * third party import

    * local application/ library specific import

* String quotes:

  * Use single quotes

  * Use double quotes if it involves single quotes within

* White spaces:

  * Avoid trailing white spaces

  * Avoid unnecessary white spaces:

    * right inside parentheses or brackets

    * after a comma followed by a closing bracket

    * before a comma, colon or semi-colon

    * before an open parentheses that has an argument


Testing
_______

Testing is an important part of Software Quality Assurance because it once again enforces the project to be the highest of quality. It allows developers to check if the software behaviours are run as expected. It reduces maintenance cost, increases functionality  and usability. Testing identifies bugs earlier in the process, so they can be fixed as soon as possible resulting in finance efficiency because each fix further down the process would cost a lot more. 

Additionally, testing also ensures good security for the project as developers can test and go through all the vulnerable situations to prevent security breach. This benefit is extremely important to this banking project because as a bank, users' information must be kept safe and secured at all times from malicious intentions.

* Unit Testing
* Integration Testing
* System Testing


Measure the test coverage
_________________________

Test coverage is a testing metric that defines test density. It gives a general idea about how much and whether a product has been properly tested. Having higher test coverage increases our chance of finding bugs and fixing them. This would ensure the codes stay high quality. Test coverage is aligned to the requirement specifications and tests the software features.

To calculate the test coverage, take the total number of lines tested divided by the total number of lines in the component then multiply by 100.

Linting
_______

Linting is used to ensure that our codes are up to the coding standards set above. Linting should point out flaws in the code in the terminal if they do not follow the standards. Developers can easily see the error messages and fix them immediately to prevent a huge load of mistakes. This increases time efficiency drastically because developers do not have to manually go through each file to spot the flaws.

This project uses pylama as a linter for the Taco Bank. It allows developers to input what error codes they would want to ignore (simply because there is no way to fix it) in the pylama.ini file. This gives developers the freedom to edit and tailor the linter to the requirements of coding standards.

Pylama identifies common errors such as: syntax, structural, unused imports, dangerous data type combinations, unreachable codes, unspecified and undefined behaviours.

The linting process works as following: 

* Developers write the code on their individual machine
* Pylama is run after each daily session of code writing
* The code is analysed by the linter
* The coders review the bugs spotted by pylama
* The bugs are fixed immediately
* These steps are repeated daily until the member is ready to push the code into their own branch on gitlab
* When pushed, gitlab will run a CI pipeline that includes automated linting job
* The lint job is consisted of having an installation and run command for pylama
* The code will be analysed by pylama again 
* The developers then review the bugs and errors identified by pylama through the terminal
* Changes will be made to the flaws 
* The code can now be pushed into the main branch

CI/CD
_____

For the Taco Bank project, the project is set up with the CI/CD pipelines on Gitlab. It is triggered every time a developer pushes their code to a branch. The structure of our pipeline is shown below:

1. Build:
    * build-job
2. Test:   
    * unit-test-job
    * lint-test-job
3. Deploy:
    * pages

