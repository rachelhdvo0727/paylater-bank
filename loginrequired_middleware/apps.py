from django.apps import AppConfig


class LoginrequiredMiddlewareConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'loginrequired_middleware'
