from __future__ import annotations
from decimal import Decimal
from django.db import models, transaction
from django.db.models import Q
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
from .errors import InsufficientFunds
from django.core.validators import RegexValidator


class UID(models.Model):
    @classmethod
    @property
    def uid(cls):
        return cls.objects.create()

    def __str__(self):
        return f'{self.pk}'


class Customer(models.Model):
    user = models.OneToOneField(
        User, primary_key=True, on_delete=models.PROTECT)
    ranking_choices = [('G', 'Gold'), ('S', 'Silver'), ('B', 'Basic'), ]
    rank = models.CharField(
        max_length=15, choices=ranking_choices, default='B', )

    # validation for numbers only
    alphanumeric = RegexValidator(r'^[0-9]*$', 'Only numbers are allowed.')
    personal_id = models.CharField(
        max_length=10, db_index=True, validators=[alphanumeric])
    phone = models.CharField(
        max_length=15, db_index=True, validators=[alphanumeric])

    @property
    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    @property
    def can_make_loan(self) -> str:
        return f'{self.rank}'

    @property
    def default_account(self) -> Account:
        return Account.objects.filter(user=self.user).first()

    def make_loan(self, amount, name, payment):
        assert amount >= 0, 'Negative amount not allowed for loan.'
        # rank = self.rank
        interest = 3
        payment = Decimal(payment)
        new_amount = (interest * amount) / (1 - pow(1 + interest, -payment))
        loan = Account.objects.create(user=self.user, name=f'Loan: {name}')
        Ledger.loan_transfer(
            new_amount,
            amount,
            loan,
            f'Loan paid out to account {self.default_account}',
            self.default_account,
            f'Credit from loan {loan.pk}: {loan.name}',
        )
#        elif (rank == 'gold'):
#            interest = 5;
#            new_amount = (interest * amount) / (1 - pow(1+interest,-payment))
#            loan = Account.objects.create(user=self.user, name=f'Loan: {name}')
#            Ledger.transfer(
#                    new_amount,
#                    loan,
#                    f'Loan paid out to account {self.default_account}',
#                    self.default_account,
#                    f'Credit from loan {loan.pk}: {loan.name}',
#                    is_loan=True
#            )

    @classmethod
    def search(cls, search_term):
        return cls.objects.filter(
            Q(user__username__contains=search_term) |
            Q(user__first_name__contains=search_term) |
            Q(user__last_name__contains=search_term) |
            Q(user__email__contains=search_term) |
            Q(personal_id__contains=search_term) |
            Q(phone__contains=search_term)
        )[:15]

    def __str__(self):
        return f'{self.personal_id}: {self.full_name}'


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=50, db_index=True)

    class Meta:
        get_latest_by = 'pk'

    @property
    def movements(self) -> QuerySet:
        return Ledger.objects.filter(account=self)

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    @balance.setter
    def balance(self, value):
        return self.balance + value

    def __str__(self):
        return f'{self.pk} :: {self.user} :: {self.name}'


class Ledger(models.Model):
    account = models.ForeignKey(
        Account, on_delete=models.PROTECT, blank=True, null=True)
    transaction = models.ForeignKey(UID, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    text = models.TextField()

    external_debit_account = models.CharField(
        max_length=100, default='Not external', blank=True, null=True)
    external_credit_account = models.CharField(
        max_length=100, default='Not external', blank=True, null=True)

    @classmethod
    def transfer(cls, amount, debit_account, debit_text, credit_account, credit_text) -> int:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        with transaction.atomic():
            if debit_account.balance >= amount:
                uid = UID.uid
                cls(amount=-amount, transaction=uid,
                    account=debit_account, text=debit_text).save()
                cls(amount=amount, transaction=uid,
                    account=credit_account, text=credit_text).save()
            else:
                raise InsufficientFunds
        return uid

    @classmethod
    def loan_transfer(cls, new_amount, amount, debit_account, debit_text, credit_account, credit_text) -> int:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        with transaction.atomic():
            uid = UID.uid
            cls(amount=-new_amount, transaction=uid,
                account=debit_account, text=debit_text).save()
            cls(amount=amount, transaction=uid,
                account=credit_account, text=credit_text).save()
            cls(amount=new_amount - amount, transaction=uid,
                account=Account.objects.get(name='Bank OPS Account'), text=credit_text).save()
        return uid

    @classmethod
    def external_transfer(cls, amount, debit_account, debit_text, credit_account, credit_text, is_loan=False) -> int:
        assert amount >= 0, 'Negative amount not allowed for "transfer.'
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                uid = UID.uid
                cls(amount=-amount, transaction=uid,
                    account=debit_account, text=debit_text).save()
                cls(amount=amount, transaction=uid,
                    external_credit_account=credit_account, text=credit_text).save()
            else:
                raise InsufficientFunds
        return uid

    @classmethod
    def deposit(cls, amount, debit_account, debit_text, credit_account, credit_text) -> int:
        assert amount >= 0, 'Negative amount not allowed for "transfer.'
        with transaction.atomic():
            uid = UID.uid
            cls(amount=-amount, transaction=uid,
                external_debit_account=debit_account, text=debit_text).save()
            cls(amount=amount, transaction=uid,
                account=credit_account, text=credit_text).save()
        return uid

    def __str__(self):
        if self.account is None:
            return f'{self.amount} :: {self.transaction} :: {self.timestamp} :: {self.external_debit_account} :: {self.external_credit_account} :: {self.text}'
        else:
            return f'{self.amount} :: {self.transaction} :: {self.timestamp} :: {self.account} :: {self.text}'


class Transaction(models.Model):
    transaction = models.IntegerField(
        blank=True, null=True)
    amount = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True)
    timestamp = models.DateTimeField(
        auto_now_add=True, db_index=True, blank=True, null=True)
    debit_account = models.CharField(max_length=100, blank=True, null=True)
    credit_account = models.CharField(max_length=100, blank=True, null=True)
    text = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.account} :: from {self.credit_account} :: to {self.debit_account} :: text {self.text} :: {self.timestamp}'
