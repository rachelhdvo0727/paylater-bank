from rest_framework import generics
from .serializers import TransactionsSerializer
from .models import Transaction


class TransactionView(generics.ListCreateAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionsSerializer
