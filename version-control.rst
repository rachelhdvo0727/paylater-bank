Version Control System
======================

This page will show how developers divide and work on their respective tasks. Taco Bank uses Git in conjunction with GitLab, a web-base repository. Each developer will clone the repository and set up locally on their machine. The main branch will be the official version of the application. When a developer takes on a task, they will be responsible to make a feature branch from the development branch, and attach their name and the name of the task/feature, as followed:

main

* feature/person1/task
* feature/person2/task

Each developer has to make sure that the local version of the project is updated as frequently as possible by running git pull. Developers have to commit and push changes on their branches frequently to be sure that there are no big changes that can lead to conflicts when changes are merged in.

If the task is done on one feature branch, the developer should run git merge main to update it by merging main’s latest code to the feature branch. Then, the local branch is ready to be merged into the main branch. This way, it minimises as many conflicts as possible.

The merging can be done either in the terminal or making a pull request on GitLab. These pull requests are in fact merge requests, which can be reviewed by another developer, if they are assigned.  

There is a staging branch implemented for testing new features with the working version before it publishes to the main branch. Developers can use the development branch to test merges and if the newly merged code does not work with the current working code base, it should not be published it to the main branch.

main |br|
development

* feature/person1/task
* feature/person2/task

Version control is further combined with other tools and tasks, such as linting and tests to ensure code formatting is uniformed overall and unit tests. 


.. |br| raw:: html

      <br>
